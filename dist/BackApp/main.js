(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _organisation_organisation_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./organisation/organisation.component */ "./src/app/organisation/organisation.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./list/list.component */ "./src/app/list/list.component.ts");





var routes = [
    {
        path: 'organisation', component: _organisation_organisation_component__WEBPACK_IMPORTED_MODULE_3__["OrganisationComponent"],
    },
    { path: 'list', component: _list_list_component__WEBPACK_IMPORTED_MODULE_4__["ListComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'BackApp';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _organisation_organisation_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./organisation/organisation.component */ "./src/app/organisation/organisation.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./list/list.component */ "./src/app/list/list.component.ts");










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _organisation_organisation_component__WEBPACK_IMPORTED_MODULE_6__["OrganisationComponent"],
                _list_list_component__WEBPACK_IMPORTED_MODULE_9__["ListComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/list/list.component.css":
/*!*****************************************!*\
  !*** ./src/app/list/list.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xpc3QvbGlzdC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/list/list.component.html":
/*!******************************************!*\
  !*** ./src/app/list/list.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input type=\"submit\" value=\"edit\" (click)=\"myFunc()\">\n"

/***/ }),

/***/ "./src/app/list/list.component.ts":
/*!****************************************!*\
  !*** ./src/app/list/list.component.ts ***!
  \****************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ListComponent = /** @class */ (function () {
    function ListComponent(router) {
        this.router = router;
    }
    ListComponent.prototype.ngOnInit = function () {
    };
    //   myFunc() {
    //     console.log('ok');
    //     this.router.navigate(['../organisation/2099/15']);
    // }
    ListComponent.prototype.myFunc = function () {
        console.log('ok');
        this.router.navigate(['/organisation'], { queryParams: { params: JSON.stringify({ args: ['2099', 15] }) } });
    };
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.css */ "./src/app/list/list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ListComponent);
    return ListComponent;
}());



/***/ }),

/***/ "./src/app/org.service.ts":
/*!********************************!*\
  !*** ./src/app/org.service.ts ***!
  \********************************/
/*! exports provided: OrgService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrgService", function() { return OrgService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var OrgService = /** @class */ (function () {
    function OrgService(router, http) {
        this.router = router;
        this.http = http;
    }
    OrgService.prototype.getOrganizationToEdit = function (params) {
        console.log(JSON.stringify(params));
        // tslint:disable-next-line:max-line-length
        return this.http.get('http://wegaaccount.com/B2BBackend/http/invoke/service/organizationBean/getOrganizationToEdit/JSON?params=' +
            JSON.stringify(params));
    };
    OrgService.prototype.myFunc = function () {
        console.log('ok');
        this.router.navigate(['../organisation/2099/15']);
    };
    OrgService.prototype.getOrganisation = function (organisationForm, organisation) {
        var org = organisation.organizationToEdit;
        console.log(org.name);
        organisationForm.value.business_name = org.name;
    };
    OrgService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], OrgService);
    return OrgService;
}());



/***/ }),

/***/ "./src/app/organisation/organisation.component.css":
/*!*********************************************************!*\
  !*** ./src/app/organisation/organisation.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL29yZ2FuaXNhdGlvbi9vcmdhbmlzYXRpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/organisation/organisation.component.html":
/*!**********************************************************!*\
  !*** ./src/app/organisation/organisation.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">\n<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js\"></script>\n<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>\n<!------ Include the above in your HEAD tag ---------->\n\n<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">\n<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\"></script>\n<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>\n<!------ Include the above in your HEAD tag ---------->\n\n<head>\n  <title>Bootstrap Example</title>\n  <meta charset=\"utf-8\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n</head>\n\n\n<hr>\n<div class=\"container bootstrap snippet\">\n  <div class=\"row\">\n    <div class=\"col-sm-10\">\n      <h1>User name</h1>\n    </div>\n    <div class=\"col-sm-2\"><a href=\"/users\" class=\"pull-right\"><img title=\"profile image\" class=\"img-circle img-responsive\"\n          src=\"http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100\"></a></div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-sm-3\">\n      <!--left col-->\n\n\n      <div class=\"text-center\">\n        <img src=\"http://ssl.gstatic.com/accounts/ui/avatar_2x.png\" class=\"avatar img-circle img-thumbnail\" alt=\"avatar\">\n        <h6>Upload a different photo...</h6>\n        <input type=\"file\" class=\"text-center center-block file-upload\">\n      </div><br>\n\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">Website <i class=\"fa fa-link fa-1x\"></i></div>\n        <div class=\"panel-body\"><a href=\"http://bootnipets.com\">bootnipets.com</a></div>\n      </div>\n\n\n      <ul class=\"list-group\">\n        <li class=\"list-group-item text-muted\">Activity <i class=\"fa fa-dashboard fa-1x\"></i></li>\n        <li class=\"list-group-item text-right\"><span class=\"pull-left\"><strong>Shares</strong></span> 125</li>\n        <li class=\"list-group-item text-right\"><span class=\"pull-left\"><strong>Likes</strong></span> 13</li>\n        <li class=\"list-group-item text-right\"><span class=\"pull-left\"><strong>Posts</strong></span> 37</li>\n        <li class=\"list-group-item text-right\"><span class=\"pull-left\"><strong>Followers</strong></span> 78</li>\n      </ul>\n\n      <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">Social Media</div>\n        <div class=\"panel-body\">\n          <i class=\"fa fa-facebook fa-2x\"></i> <i class=\"fa fa-github fa-2x\"></i> <i class=\"fa fa-twitter fa-2x\"></i>\n          <i class=\"fa fa-pinterest fa-2x\"></i> <i class=\"fa fa-google-plus fa-2x\"></i>\n        </div>\n      </div>\n\n    </div>\n    <!--/col-3-->\n    <div class=\"col-sm-9\">\n      <ul class=\"nav nav-tabs\">\n        <li class=\"active\"><a data-toggle=\"tab\" href=\"#home\">Home</a></li>\n        <li><a data-toggle=\"tab\" href=\"#messages\">Menu 1</a></li>\n        <li><a data-toggle=\"tab\" href=\"#settings\">Menu 2</a></li>\n      </ul>\n\n\n      <div class=\"tab-content\">\n        <div class=\"tab-pane active\" id=\"home\">\n          <hr>\n          <form class=\"form\" [formGroup]=\"organisationForm\" >\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"first_name\">\n                  <h4>Business name</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" formControlName=\"name\" placeholder=\"Business Name\" title=\"enter your first name if any.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"last_name\">\n                  <h4>Office ID</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"office_id\" placeholder=\"Office ID\" title=\"enter your last name if any.\">\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"phone\">\n                  <h4>Customer</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"customer\" placeholder=\"Customer\" title=\"enter your phone number if any.\">\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <div class=\"col-xs-6\">\n                <label for=\"mobile\">\n                  <h4>Cust Code</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"cust_code\" placeholder=\"Cust Code\" title=\"enter your mobile number if any.\">\n              </div>\n            </div>\n\n\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"phone\">\n                  <h4>Customer</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"customer\" placeholder=\"Customer\" title=\"enter your phone number if any.\">\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <div class=\"col-xs-6\">\n                <label for=\"mobile\">\n                  <h4>Customer/Supplier</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"cust\" placeholder=\"Customer/Supplier\" title=\"enter your mobile number if any.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"email\">\n                  <h4>Start Date</h4>\n                </label>\n                <input type=\"email\" class=\"form-control\" name=\"start_date\" placeholder=\"Start Date\" title=\"enter your email.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"email\">\n                  <h4>End Date</h4>\n                </label>\n                <input type=\"email\" class=\"form-control\" name=\"end_date\" placeholder=\"End Date\" title=\"enter your email.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"password\">\n                  <h4>Password</h4>\n                </label>\n                <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\" placeholder=\"password\" title=\"enter your password.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"password2\">\n                  <h4>Verify</h4>\n                </label>\n                <input type=\"password\" class=\"form-control\" name=\"password2\" id=\"password2\" placeholder=\"password2\"\n                  title=\"enter your password2.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <div class=\"col-xs-12\">\n                <br>\n                <button class=\"btn btn-lg btn-success\" type=\"submit\"><i class=\"glyphicon glyphicon-ok-sign\"></i> Save</button>\n                <button class=\"btn btn-lg\" type=\"reset\"><i class=\"glyphicon glyphicon-repeat\"></i> Reset</button>\n              </div>\n            </div>\n          </form>\n\n          <hr>\n\n        </div>\n        <!--/tab-pane-->\n        <div class=\"tab-pane\" id=\"messages\">\n\n          <h2></h2>\n\n          <hr>\n          <form class=\"form\" action=\"##\" method=\"post\" id=\"registrationForm\">\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"first_name\">\n                  <h4>First name</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"first_name\" id=\"first_name\" placeholder=\"first name\"\n                  title=\"enter your first name if any.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"last_name\">\n                  <h4>Last name</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"last_name\" id=\"last_name\" placeholder=\"last name\" title=\"enter your last name if any.\">\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"phone\">\n                  <h4>Phone</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"phone\" id=\"phone\" placeholder=\"enter phone\" title=\"enter your phone number if any.\">\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <div class=\"col-xs-6\">\n                <label for=\"mobile\">\n                  <h4>Mobile</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"mobile\" id=\"mobile\" placeholder=\"enter mobile number\"\n                  title=\"enter your mobile number if any.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"email\">\n                  <h4>Email</h4>\n                </label>\n                <input type=\"email\" class=\"form-control\" name=\"email\" id=\"email\" placeholder=\"you@email.com\" title=\"enter your email.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"email\">\n                  <h4>Location</h4>\n                </label>\n                <input type=\"email\" class=\"form-control\" id=\"location\" placeholder=\"somewhere\" title=\"enter a location\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"password\">\n                  <h4>Password</h4>\n                </label>\n                <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\" placeholder=\"password\" title=\"enter your password.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"password2\">\n                  <h4>Verify</h4>\n                </label>\n                <input type=\"password\" class=\"form-control\" name=\"password2\" id=\"password2\" placeholder=\"password2\"\n                  title=\"enter your password2.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <div class=\"col-xs-12\">\n                <br>\n                <button class=\"btn btn-lg btn-success\" type=\"submit\"><i class=\"glyphicon glyphicon-ok-sign\"></i> Save</button>\n                <button class=\"btn btn-lg\" type=\"reset\"><i class=\"glyphicon glyphicon-repeat\"></i> Reset</button>\n              </div>\n            </div>\n          </form>\n\n        </div>\n        <!--/tab-pane-->\n        <div class=\"tab-pane\" id=\"settings\">\n\n\n          <hr>\n          <form class=\"form\" action=\"##\" method=\"post\" id=\"registrationForm\">\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"first_name\">\n                  <h4>First name</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"first_name\" id=\"first_name\" placeholder=\"first name\"\n                  title=\"enter your first name if any.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"last_name\">\n                  <h4>Last name</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"last_name\" id=\"last_name\" placeholder=\"last name\" title=\"enter your last name if any.\">\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"phone\">\n                  <h4>Phone</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"phone\" id=\"phone\" placeholder=\"enter phone\" title=\"enter your phone number if any.\">\n              </div>\n            </div>\n\n            <div class=\"form-group\">\n              <div class=\"col-xs-6\">\n                <label for=\"mobile\">\n                  <h4>Mobile</h4>\n                </label>\n                <input type=\"text\" class=\"form-control\" name=\"mobile\" id=\"mobile\" placeholder=\"enter mobile number\"\n                  title=\"enter your mobile number if any.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"email\">\n                  <h4>Email</h4>\n                </label>\n                <input type=\"email\" class=\"form-control\" name=\"email\" id=\"email\" placeholder=\"you@email.com\" title=\"enter your email.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"email\">\n                  <h4>Location</h4>\n                </label>\n                <input type=\"email\" class=\"form-control\" id=\"location\" placeholder=\"somewhere\" title=\"enter a location\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"password\">\n                  <h4>Password</h4>\n                </label>\n                <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\" placeholder=\"password\" title=\"enter your password.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n\n              <div class=\"col-xs-6\">\n                <label for=\"password2\">\n                  <h4>Verify</h4>\n                </label>\n                <input type=\"password\" class=\"form-control\" name=\"password2\" id=\"password2\" placeholder=\"password2\"\n                  title=\"enter your password2.\">\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <div class=\"col-xs-12\">\n                <br>\n                <button class=\"btn btn-lg btn-success pull-right\" type=\"submit\"><i class=\"glyphicon glyphicon-ok-sign\"></i>\n                  Save</button>\n                <!--<button class=\"btn btn-lg\" type=\"reset\"><i class=\"glyphicon glyphicon-repeat\"></i> Reset</button>-->\n              </div>\n            </div>\n          </form>\n        </div>\n\n      </div>\n      <!--/tab-pane-->\n    </div>\n    <!--/tab-content-->\n\n  </div>\n  <!--/col-9-->\n</div>\n<!--/row-->\n"

/***/ }),

/***/ "./src/app/organisation/organisation.component.ts":
/*!********************************************************!*\
  !*** ./src/app/organisation/organisation.component.ts ***!
  \********************************************************/
/*! exports provided: OrganisationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrganisationComponent", function() { return OrganisationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _org_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../org.service */ "./src/app/org.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");





var OrganisationComponent = /** @class */ (function () {
    function OrganisationComponent(orgService, router) {
        var _this = this;
        this.orgService = orgService;
        this.router = router;
        this.organisationForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
        });
        this.router.queryParams.subscribe(function (params) {
            console.log(params);
            _this.params = JSON.parse(params.params);
            console.log(JSON.parse(params.params));
        });
    }
    OrganisationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.orgService.getOrganizationToEdit(JSON.parse(this.params)).subscribe(function (data) {
            _this.organisationForm.controls['name'].setValue(data.organizationToEdit.name);
        });
    };
    OrganisationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-organisation',
            template: __webpack_require__(/*! ./organisation.component.html */ "./src/app/organisation/organisation.component.html"),
            styles: [__webpack_require__(/*! ./organisation.component.css */ "./src/app/organisation/organisation.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_org_service__WEBPACK_IMPORTED_MODULE_2__["OrgService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], OrganisationComponent);
    return OrganisationComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\RANIM\Downloads\BackApp-master\BackApp-master\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map