FROM nginx:alpine
LABEL author='ranim'
COPY ./nginx.config /etc/nginx/conf.d/default.conf
COPY ./dist /var/www/html/ranim